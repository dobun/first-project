<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
    <? $APPLICATION->ShowHead();
	use Bitrix\Main\Page\Asset;
	?>
	<link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">
	<?
    // CSS
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/bootstrap.min.css');
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/slick.css');
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/slick-theme.css');
 	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/nouislider.min.css');
	 Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/font-awesome.min.css');
	 Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/style.css');
	 
	 
	 // JS
	 // CJSCore::Init(array('jquery'));
	 Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery.min.js');
	 Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/bootstrap.min.js');
	 Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/slick.min.js');
	 Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/nouislider.min.js');
	 Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery.zoom.min.js');
	 Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/main.js');
	 
	 
	 ?>
	 


</head>
<body>
	<?echo SITE_TEMPLATE_PATH.'/css/bootstrap.min.css'?>
<? $APPLICATION-> ShowPanel(); ?>
		<?// ОТОБРАЖЕНИЕ ВЫБРАННОГО КОЛ-ВА ТОВАРОВ В HEADER 
		use Bitrix\Main\Loader;
			Loader::includeModule("sale");
			$delaydBasketItems = CSaleBasket::GetList(
				array(),
					array(
					"FUSER_ID" => CSaleBasket::GetBasketUserID(),
					"LID" => SITE_ID,
					"ORDER_ID" => "NULL",
					"DELAY" => "Y"
				),
			array()
			);
			echo $delaydBasketItems;
		?>
	<!-- HEADER -->
	<header>
		<!-- top Header -->
		<div id="top-header">
			<div class="container">
				<div class="pull-left">
					<span>Welcome to E-shop!</span>
				</div>
				<div class="pull-right">
					<ul class="header-top-links">
						<li><a href="#">Store</a></li>
						<li><a href="#">Newsletter</a></li>
						<li><a href="#">FAQ</a></li>
						<li class="dropdown default-dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">ENG <i class="fa fa-caret-down"></i></a>
							<ul class="custom-menu">
								<li><a href="#">English (ENG)</a></li>
								<li><a href="#">Russian (Ru)</a></li>
								<li><a href="#">French (FR)</a></li>
								<li><a href="#">Spanish (Es)</a></li>
							</ul>
						</li>
						<li class="dropdown default-dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">USD <i class="fa fa-caret-down"></i></a>
							<ul class="custom-menu">
								<li><a href="#">USD ($)</a></li>
								<li><a href="#">EUR (€)</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- /top Header -->


		<!-- header -->
		<div id="header">
			<div class="container">
				<div class="pull-left">
					<!-- Logo -->
					<div class="header-logo">
						<a class="logo" href="#">
							<img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" alt="/">
						</a>
					</div>
					<!-- /Logo -->

					<!-- Search -->
					<div class="header-search">
						<form>
							<input class="input search-input" type="text" placeholder="Enter your keyword">
							<select class="input search-categories">
								<option value="0">All Categories</option>
								<option value="1">Category 01</option>
								<option value="1">Category 02</option>
							</select>
							<button class="search-btn"><i class="fa fa-search"></i></button>
						</form>
					</div>
					<!-- /Search -->
				</div>
				<div class="pull-right">
					<ul class="header-btns">
						<li class="header-account dropdown default-dropdown">
					
					
					<!-- Account -->
						
						
						
					<div class="dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="true">




<!-- условие показа элемента для авторизованный пользователей  -->
<?/*
if(!$USER->IsAuthorized()) { 
   echo TruncateText($arResult["DETAIL_TEXT"], 200);
} else { 
   echo $arResult["DETAIL_TEXT"];
} */?>
							<!-- компонент авторизации -->
							<?/*
								$APPLICATION->IncludeComponent(
									"bitrix:system.auth.form", 
									".default", 
									array(
										"FORGOT_PASSWORD_URL" => "/auth/?forgot_password=yes", //="/auth/?register=yes" 
										"PROFILE_URL" => "/personal/",
										"REGISTER_URL" => "/auth/?register=yes",
										"SHOW_ERRORS" => "Y",
										"COMPONENT_TEMPLATE" => ".default"
									),
									false
								);
							*/?>



			










								<div class="header-btns-icon">
									<i class="fa fa-user-o"></i>
								</div>
								<strong class="text-uppercase">My Account <i class="fa fa-caret-down"></i></strong>
							</div>

							<?/*if ($USER->IsAuthorized()):?>

								<a href="<?echo $APPLICATION->GetCurPageParam("logout=yes", array(
								"login",
								"logout",
								"register",
								"forgot_password",
								"change_password"));?>">LOGOUT</a> 

								<?else:?>

								<a href="<?echo $APPLICATION->GetCurPageParam("register=yes", array(
								"login",
								"logout",
								"forgot_password",
								"change_password"));?>">LOGIN</a>
							<?endif;*/?>
/

							 <a href="/auth/?login=yes" class="text-uppercase" >Login</a>
							<!-- /
							<a href="" class="text-uppercase" >Logout</a>
							/							 -->
							  <a href="/auth/?register=yes" class="text-uppercase">Join</a>
							<ul class="custom-menu">
								<?$APPLICATION->IncludeComponent("bitrix:menu", "account_dropdown_menu", Array(
									"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
										"CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
										"DELAY" => "N",	// Откладывать выполнение шаблона меню
										"MAX_LEVEL" => "1",	// Уровень вложенности меню
										"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
										"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
										"MENU_CACHE_TYPE" => "N",	// Тип кеширования
										"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
										"ROOT_MENU_TYPE" => "",	// Тип меню для первого уровня
										"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
										"COMPONENT_TEMPLATE" => ".default"
									),
									false
								);?>

								<!-- <li><a href="#"><i class="fa fa-user-o"></i> My Account</a></li>
								<li><a href="#"><i class="fa fa-heart-o"></i> My Wishlist</a></li>
								<li><a href="#"><i class="fa fa-exchange"></i> Compare</a></li>
								<li><a href="#"><i class="fa fa-check"></i> Checkout</a></li>
								<li><a href="#"><i class="fa fa-unlock-alt"></i> Login </a></li>
								<li><a href="#"><i class="fa fa-user-plus"></i> Create An Account</a></li> -->
							</ul>  



 
						<!-- /Account -->
						</li>

						<!-- Cart -->
						<li class="header-cart dropdown default-dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
								<div class="header-btns-icon">
									<i class="fa fa-shopping-cart"></i>
									<span class="qty">
										<? // ОТОБРАЖЕНИЕ ВЫБРАННОГО КОЛ-ВА ТОВАРОВ В HEADER 
											echo $delaydBasketItems;?>
									</span>
								</div>
								<strong class="text-uppercase">My Cart:</strong>
								<br>
								<span>35.20$</span>
							</a>
							<div class="custom-menu">
								<div id="shopping-cart">
									<div class="shopping-cart-list">
										<div class="product product-widget">
											<div class="product-thumb">
												<img src="<?=SITE_TEMPLATE_PATH?>/img/thumb-product01.jpg" alt="">
											</div>
											<div class="product-body">
												<h3 class="product-price">$32.50 <span class="qty">x3</span></h3>
												<h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
											</div>
											<button class="cancel-btn"><i class="fa fa-trash"></i></button>
										</div>
										<div class="product product-widget">
											<div class="product-thumb">
												<img src="<?=SITE_TEMPLATE_PATH?>/img/thumb-product01.jpg" alt="">
											</div>
											<div class="product-body">
												<h3 class="product-price">$32.50 <span class="qty">x3</span></h3>
												<h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
											</div>
											<button class="cancel-btn"><i class="fa fa-trash"></i></button>
										</div>
									</div>
									<div class="shopping-cart-btns">
										<button class="main-btn">View Cart</button>
										<button class="primary-btn">Checkout <i class="fa fa-arrow-circle-right"></i></button>
									</div>
								</div>
							</div>
						</li>
						<!-- /Cart -->

						<!-- Mobile nav toggle-->
						<li class="nav-toggle">
							<button class="nav-toggle-btn main-btn icon-btn"><i class="fa fa-bars"></i></button>
						</li>
						<!-- / Mobile nav toggle -->
					</ul>
				</div>
			</div>
			<!-- header -->
		</div>
		<!-- container -->
	</header>
	<!-- /HEADER -->

	<!-- NAVIGATION -->
	<div id="navigation">
		<!-- container -->
		<div class="container">
			<div id="responsive-nav">
				<!-- category nav -->
				<div class="category-nav">
					<span class="category-header">Categories <i class="fa fa-list"></i></span>
					<ul class="category-list">


<?// php if($_SERVER['REQUEST_URI'] === '/'): ?>


<!-- ЛЕВОЕ МЕНЮ -->
	
			<?php if($_SERVER['REQUEST_URI'] === '/' ): ?>
			

			
				<?$APPLICATION->IncludeComponent(
				"bitrix:catalog.section.list", 
				"template1", 
				array(
					"ADD_SECTIONS_CHAIN" => "Y",
					"CACHE_GROUPS" => "Y",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"COUNT_ELEMENTS" => "N",
					"IBLOCK_ID" => "3",
					"IBLOCK_TYPE" => "dinamic_content",
					"SECTION_CODE" => "",
					"SECTION_FIELDS" => array(
						0 => "",
						1 => "",
					),
					"SECTION_ID" => $_REQUEST["SECTION_ID"],
					"SECTION_URL" => "",
					"SECTION_USER_FIELDS" => array(
						0 => "",
						1 => "",
					),
					"SHOW_PARENT_NAME" => "Y",
					"TOP_DEPTH" => "1",
					"VIEW_MODE" => "LINE",
					"COMPONENT_TEMPLATE" => "template1"
				),
				false
);?>
					
<?php endif; ?>					
					</ul>
				</div>
				<!-- /category nav -->







				
				<!-- menu nav -->
				<div class="menu-nav">
					<span class="menu-header">Menu <i class="fa fa-bars"></i></span>
					<ul class="menu-list">






						<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"horizontal_menu", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "top",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "horizontal_menu"
	),
	false
);?>


					</ul>
				</div>
				<!-- menu nav -->
			</div>
		</div>
		<!-- /container -->
	</div>
	<!-- /NAVIGATION -->

	<!-- HOME -->
	<div id="home">
		<!-- container -->
		<div class="container">
			<!-- home wrap -->
			<div class="home-wrap">
				<!-- home slick -->
			
				<!-- start slider -->
				<?php if($_SERVER['REQUEST_URI'] === '/'): ?>



				<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"banner_home_slide", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "banner_home_slide",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "dinamic_content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "3",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	),
	false
);?>
<?php endif; ?>
<!-- end slider -->
							<!-- /home slick -->
						
					</div>
				</div>

	<div id="breadcrumb">
		<div class="container">
			<ul class="breadcrumb">
			<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breads_crumb", Array(
	"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
		"SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
		"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
	),
	false
);?>
			</ul>
		</div>
	</div>

			</div>

			<script src="<?=SITE_TEMPLATE_PATH?>/js/bootstrap.min.js"></script>