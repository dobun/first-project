<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div id="home-slick">

<?foreach($arResult["ITEMS"] as $arItem):?>
<!-- перебор массива -->
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	// визуальное редактирование?>
	<div class="banner banner-1" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	<!-- событие при котором включается визуальное реадактирование	 -->
	<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
		<div class="banner-caption text-center">
			<h1><?=$arItem['NAME']?></h1>
			<h3 class="white-color font-weak"><?=$arItem['PREVIEW_TEXT']?></h3>
			<button class="primary-btn">Shop Now</button>
		</div>
	</div>
<?endforeach;?>

</div>


<?
// echo "<pre>";
// print_r($arResult);
// echo "</pre>";
?>
