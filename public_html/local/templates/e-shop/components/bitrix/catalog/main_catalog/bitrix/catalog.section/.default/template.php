<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>


<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>


<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
		<?
		$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
		?>
 
 <!-- BASE -->




<?//echo "<pre>"; print_r($arResult['ITEMS']['0']['PREVIEW_PICTURE']['SRC']); echo "</pre>";?>

<div id="<?=$this->GetEditAreaId($arElement['DETAIL PICTURE']);?>">

	<div class='col-md-4 col-sm-6 col-xs-6'>
		<div class="product product-single">
			<div class="product-thumb">
				<div class="product-label">
					<?if($arElement["PRICES"]["BASE"]["DISCOUNT_DIFF_PERCENT"] > 0):?>
						<span>Sale</span>
						<span class="sale">-<?=$arElement["DETAIL_PICTURE"]?>%</span>
						<!-- <ul class="product-countdown">
							<li><span>00 H</span></li>
							<li><span>00 M</span></li>
							<li><span>00 S</span></li>
						</ul> -->
					<?endif;?>
				</div>
				<?//echo "<pre>"; print_r($arResult); echo "</pre>";?>
				<button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
				<img src="<?=$arResult['ITEMS']['0']['PREVIEW_PICTURE']['SRC']?>" alt="">
			</div>
			<div class="product-body">	
				<?foreach($arElement["PRICES"] as $code=>$arPrice):?>
					<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
						<h3 class="product-price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?><del class="product-old-price"><?=$arPrice["PRINT_VALUE"]?></del></h3>
					<?else:?>
						<h3 class="product-price"><?=$arPrice["PRINT_VALUE"]?></h3>
					<?endif?>
					<br>
					<div class="product-rating">
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star-o empty"></i>
					</div> 
					<?endforeach?>
					<h2 class="product-name"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></h2>
				<div class="product-btns">
					<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
					<button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
					<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
				</div>
			</div>
		</div>
	</div>

</div>

<?endforeach?>
