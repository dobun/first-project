(function (window) {
    'use strict';

    if (window.JCCatalogElement)
        return;

    var BasketButton = function (params) {
        BasketButton.superclass.constructor.apply(this, arguments);
        this.buttonNode = BX.create('SPAN', {
            props: { className: 'btn btn-default btn-buy btn-sm', id: this.id },
            style: typeof params.style === 'object' ? params.style : {},
            text: params.text,
            events: this.contextEvents
        });

        if (BX.browser.IsIE()) {
            this.buttonNode.setAttribute('hideFocus', 'hidefocus');
        }
    };
    BX.extend(BasketButton, BX.PopupWindowButton);
    // конструктор




    window.JCCatalogElement = function (arParams) {
        //console.log(arParams);
        //BX.proxy(this._нвзвание функции_, this)
        // var nameNode = document.getElementById('example');
        // nameNode.innerText = arParams.PRODUCT.NAME;
        // console.log(nameNode);
        this.params = arParams;
        this.offers = this.params.OFFERS;
        this.offer_num = 0;

        this.event();
        this.initSlider();
        this.loadedDetailPicture();
        // var arOffers = arParams['OFFERS'];
        // console.log(arOffers[0]['TREE']); // перебрать

        // var data;
        // var offer_num = 0;
        // var ar = data.split('_');
        // ar[0] = ID + PROP_ID;
        // ar[1] = VALUE;




        // for (i = 0; i < arOffers.legth; i++) {
        //     if (arOffers[i]['TREE'][key] = ar[1]) {
        //         offer_num = ArOffers[i]['ID']
        //     }
        // }





        //     this.obProduct = null;
        //     this.quantity = 1;
        //     this.obQuantity = null;
        //     this.obQuantityUp = null;
        //     this.obQuantityDown = null;
        //     this.obBasketAdd = null;
        //     this.obPrice = {
        //         price: null,
        //         full: null,
        //         discount: null,
        //         percent: null,
        //         total: null


        //     this.productType = 0;
        //     this.currentPrices = [];
        //     this.currentPriceSelected = 0;
        //     this.visual = {};
        //     this.treeProps = [];
        //     this.selectedValues = {};
        //     this.obPopupWin = null;
        //     };
        //     this.product = {
        //         checkQuantity: false,
        //         maxQuantity: 0,
        //         stepQuantity: 1,
        //         startQuantity: 1,
        //         isDblQuantity: false,
        //         canBuy: true,
        //         canSubscription: true,
        //         name: '',
        //         pict: {},
        //         id: 0,
        //         addUrl: '',
        //         buyUrl: '',
        //         slider: {},
        //         sliderCount: 0,
        //         useSlider: false,
        //         sliderPict: []
        //     };
        //     this.basketData = {
        //         basketUrl: '',
        //         add_url: '',
        //         buy_url: ''
        //     };

        //     if (typeof arParams === 'object') {
        //         this.params = arParams;
        //     }

        //     this.init();

        //     switch (this.productType) {
        //         case 0: // no catalog
        //         case 1: // product
        //         case 2: // set
        //             this.initProductData();
        //             break;
        //         case 3: // sku
        //             this.initOffersData();
        //             break;
        //         default:
        //             this.errorCode = -1;
        //     }
    };

    window.JCCatalogElement.prototype = {

        searchOffer: function (value) {
            var ar, property_key, property_value, offer_num = 0;

            ar = value.split('_');
            property_key = 'PROP_' + ar[0];
            property_value = ar[1];

            for (let i = 0; i < this.offers.length; i++) {
                if (this.offers[i]['TREE'][property_key] == property_value) {
                    this.offer_num = i;
                    break;

                }
            }
            this.test();
        },

        test: function () {
            console.log(this.offers[this.offer_num]);
            var offer = this.offers[this.offer_num];
            console.log(offer.DETAIL_PICTURE.SRC);
            this.unSlick();
            $('#product-main-view img').attr({ 'src': offer.DETAIL_PICTURE.SRC });
            this.initSlider();
        },
        event: function () {
            $(document).on('click', '.color-option li', BX.proxy(function (e) {
                console.log(e);
                if ($(e.target).hasClass('this_one')) {
                    var end = e.target.getAttribute('data-treevalue');
                } else {
                    var end = $(e.target).parents('li').attr('data-treevalue');
                }

                //  $(target).text('Test');
                console.log(end);
                this.searchOffer(end);
            }, this))
        },

        initSlider: function () {

            $('#product-main-view').slick({
                infinite: true,
                speed: 300,
                dots: false,
                arrows: true,
                fade: true,
                asNavFor: '#product-view',
            });

            $('#product-view').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
                centerMode: true,
                focusOnSelect: true,
                asNavFor: '#product-main-view',
            });

        },

        unSlick: function () {
            $('#product-main-view').slick('unslick');
            $('#product-view').slick('unslick');
        },

        /*      startDetaliPicture: function () {
                  //        $(document).on('click', '.color-option li', BX.proxy(function (e) {
                  $(document)on("DOMContentLoaded", ready);
      
                  console.log(e);
                  if ($(e.target).hasClass('this_one')) {
                      var end = e.target.getAttribute('data-treevalue');
                  } else {
      
                      var end = $(e.target).parents('li').attr('data-treevalue');
                  }
      
                  //  $(target).text('Test');
                  console.log(end);
                  this.searchOffer(end);
              }, this))
          },
      */
        setCompared: function (state) {
            // if (!this.obCompare)
            //     return;

            // var checkbox = this.getEntity(this.obCompare, 'compare-checkbox');
            // if (checkbox) {
            //     checkbox.checked = state;
            // }
        },

        /*
        loadedDetailPicture: function () {
            document.addEventListener("DOMContentLoaded", ready);
            console.log('hey')
        }
        */

        //     function tradeOffers() {
        //     alert('0000000000');
        // }
        // var vodka = document.getElementById('example');
        // vodka.onclick = function () {
        //     tradeOffers();
        // }















        // getEntities: function (parent, entity, additionalFilter) {
        //     if (!parent || !entity)
        //         return { length: 0 };

        //     additionalFilter = additionalFilter || '';

        //     return parent.querySelectorAll(additionalFilter + '[data-entity="' + entity + '"]');
        // },










        //     getEntities: function (parent, entity, additionalFilter) {
        //         if (!parent || !entity)
        //             return { length: 0 };

        //         additionalFilter = additionalFilter || '';

        //         return parent.querySelectorAll(additionalFilter + '[data-entity="' + entity + '"]');
        //     },

        //     init: function () {
        //         if (this.params.PRODUCT_TYPE) {
        //             this.productType = parseInt(this.params.PRODUCT_TYPE, 10);
        //         }

        //         if (this.params.BASKET) {
        //             this.basketData.basketUrl = this.params.BASKET.BASKET_URL;
        //             this.basketData.add_url = this.params.BASKET.ADD_URL_TEMPLATE;
        //             this.basketData.buy_url = this.params.BASKET.BUY_URL_TEMPLATE;
        //         }

        //         this.visual = this.params.VISUAL;

        //         this.obProduct = BX(this.visual.ID);
        //         this.obPrice.price = BX(this.visual.PRICE_ID);
        //         this.obPrice.full = BX(this.visual.OLD_PRICE_ID);
        //         this.obTree = BX(this.visual.TREE_ID);

        //         this.obQuantity = BX(this.visual.QUANTITY_ID);
        //         this.obQuantityUp = BX(this.visual.QUANTITY_UP_ID);
        //         this.obQuantityDown = BX(this.visual.QUANTITY_DOWN_ID);
        //         this.obBasketAdd = BX(this.visual.ADD_BASKET_LINK);

        //         this.obQuantityChange = BX(this.visual.CHANGE_QUANTITY);
        //         console.log(this.obQuantityChange)


        //         BX.bind(this.obQuantityChange, 'input', BX.proxy(this.changeQuantyti, this));
        //         // BX.bind(this.obQuantityUp, 'click', BX.proxy(this.quantityUp, this));
        //         // BX.bind(this.obQuantityDown, 'click', BX.proxy(this.quantityDown, this));
        //         // BX.bind(this.obQuantityDown, 'click', BX.proxy(this.quantityDown, this));
        //         BX.bind(this.obBasketAdd, 'click', BX.proxy(this.sendToBasket, this));

        //         if (this.obTree) {
        //             BX.bindDelegate(
        //                 this.obTree, 'click', { tagName: 'DIV', className: 'block-click' }, BX.proxy(this.dropDownProps, this)
        //             );

        //             var treeItems = this.obTree.querySelectorAll('[data-treevalue]'), i;
        //             for (i = 0; i < treeItems.length; i++) {
        //                 BX.bind(treeItems[i], 'click', BX.delegate(this.selectOfferProp, this));
        //             }
        //         }
        //     },

        //     changeQuantyti: function (event) {
        //         var target = event.target;
        //         this.quantity = target.value
        //     },

        //     initBasketUrl: function () {
        //         this.basketUrl = this.basketData.add_url;

        //         switch (this.productType) {
        //             case 1: // product
        //             case 2: // set
        //                 this.basketUrl = this.basketUrl.replace('#ID#', this.product.id.toString());
        //                 break;
        //             case 3: // sku
        //                 this.basketUrl = this.basketUrl.replace('#ID#', this.offers[this.offerNum].ID);
        //                 break;
        //         }

        //         this.basketParams = {
        //             'ajax_basket': 'Y',
        //             'quantity': this.quantity
        //         };
        //     },

        //     sendToBasket: function () {
        //         this.initBasketUrl();
        //         console.log(this.basketUrl);
        //         BX.ajax({
        //             method: 'POST',
        //             dataType: 'json',
        //             url: this.basketUrl,
        //             data: this.basketParams,
        //             onsuccess: BX.proxy(this.basketResult, this)
        //         });
        //     },

        //     basketResult: function (arResult) {
        //         var popupContent, popupButtons;

        //         if (arResult.STATUS === 'OK') {
        //             BX.onCustomEvent('OnBasketChange');
        //         }
        //         else {
        //             this.initPopupWindow();
        //             popupContent = '<div style="width: 100%; margin: 0; text-align: center;"><p>'
        //                 + (arResult.MESSAGE ? arResult.MESSAGE : BX.message('BASKET_UNKNOWN_ERROR'))
        //                 + '</p></div>';
        //             popupButtons = [
        //                 new BasketButton({
        //                     text: BX.message('BTN_MESSAGE_CLOSE'),
        //                     events: {
        //                         click: BX.delegate(this.obPopupWin.close, this.obPopupWin)
        //                     }
        //                 })
        //             ];

        //             this.obPopupWin.setTitleBar('Ошибка');
        //             this.obPopupWin.setContent(popupContent);
        //             this.obPopupWin.setButtons(popupButtons);
        //             this.obPopupWin.show();
        //         }
        //     },

        //     selectOfferProp: function () {
        //         var i = 0,
        //             strTreeValue = '',
        //             arTreeItem = [],
        //             rowItems = null,
        //             target = BX.proxy_context,
        //             smallCardItem;

        //         if (target && target.hasAttribute('data-treevalue')) {
        //             if (BX.hasClass(target, 'active'))
        //                 return;

        //             if (typeof document.activeElement === 'object') {
        //                 document.activeElement.blur();
        //             }

        //             strTreeValue = target.getAttribute('data-treevalue');
        //             arTreeItem = strTreeValue.split('_');
        //             this.searchOfferPropIndex(arTreeItem[0], arTreeItem[1]);
        //             rowItems = BX.findChildren(target.parentNode, { tagName: 'span' }, false);

        //             if (rowItems && rowItems.length) {
        //                 for (i = 0; i < rowItems.length; i++) {
        //                     BX.removeClass(rowItems[i], 'active');
        //                 }
        //             }

        //             BX.addClass(target, 'active');
        //         }
        //     },

        //     searchOfferPropIndex: function (strPropID, strPropValue) {
        //         var strName = '',
        //             arShowValues = false,
        //             arCanBuyValues = [],
        //             allValues = [],
        //             index = -1,
        //             i, j,
        //             arFilter = {},
        //             tmpFilter = [];

        //         for (i = 0; i < this.treeProps.length; i++) {
        //             if (this.treeProps[i].ID === strPropID) {
        //                 index = i;
        //                 break;
        //             }
        //         }

        //         if (index > -1) {
        //             for (i = 0; i < index; i++) {
        //                 strName = 'PROP_' + this.treeProps[i].ID;
        //                 arFilter[strName] = this.selectedValues[strName];
        //             }

        //             strName = 'PROP_' + this.treeProps[index].ID;
        //             arFilter[strName] = strPropValue;

        //             for (i = index + 1; i < this.treeProps.length; i++) {
        //                 strName = 'PROP_' + this.treeProps[i].ID;
        //                 arShowValues = this.getRowValues(arFilter, strName);

        //                 if (!arShowValues)
        //                     break;

        //                 allValues = [];
        //                 arCanBuyValues = arShowValues;


        //                 if (this.selectedValues[strName] && BX.util.in_array(this.selectedValues[strName], arCanBuyValues)) {
        //                     arFilter[strName] = this.selectedValues[strName];
        //                 }
        //                 else {
        //                     arFilter[strName] = arCanBuyValues[0];
        //                 }

        //                 this.updateRow(i, arFilter[strName], arShowValues, arCanBuyValues);
        //             }

        //             this.selectedValues = arFilter;
        //             this.changeInfo();
        //             console.log(this.offerNum)
        //         }

        //     },

        //     updateRow: function (intNumber, activeId, showId, canBuyId) {
        //         var i = 0,
        //             value = '',
        //             isCurrent = false,
        //             rowItems = null;

        //         var lineContainer = this.getEntities(this.obTree, 'sku-line-block');

        //         if (intNumber > -1 && intNumber < lineContainer.length) {
        //             rowItems = lineContainer[intNumber].querySelectorAll('span');
        //             for (i = 0; i < rowItems.length; i++) {
        //                 value = rowItems[i].getAttribute('data-onevalue');
        //                 isCurrent = value === activeId;

        //                 if (isCurrent) {
        //                     BX.addClass(rowItems[i], 'active');
        //                 }
        //                 else {
        //                     BX.removeClass(rowItems[i], 'active');
        //                 }

        //                 if (BX.util.in_array(value, canBuyId)) {
        //                     BX.removeClass(rowItems[i], 'notallowed');
        //                 }
        //                 else {
        //                     BX.addClass(rowItems[i], 'notallowed');
        //                 }

        //                 rowItems[i].style.display = BX.util.in_array(value, showId) ? '' : 'none';

        //                 if (isCurrent) {
        //                     lineContainer[intNumber].style.display = (value == 0 && canBuyId.length == 1) ? 'none' : '';
        //                 }
        //             }
        //         }
        //     },

        //     getRowValues: function (arFilter, index) {
        //         var arValues = [],
        //             i = 0,
        //             j = 0,
        //             boolSearch = false,
        //             boolOneSearch = true;

        //         if (arFilter.length === 0) {
        //             for (i = 0; i < this.offers.length; i++) {
        //                 if (!BX.util.in_array(this.offers[i].TREE[index], arValues)) {
        //                     arValues[arValues.length] = this.offers[i].TREE[index];
        //                 }
        //             }
        //             boolSearch = true;
        //         }
        //         else {
        //             for (i = 0; i < this.offers.length; i++) {
        //                 boolOneSearch = true;

        //                 for (j in arFilter) {
        //                     if (arFilter[j] !== this.offers[i].TREE[j]) {
        //                         boolOneSearch = false;
        //                         break;
        //                     }
        //                 }

        //                 if (boolOneSearch) {
        //                     if (!BX.util.in_array(this.offers[i].TREE[index], arValues)) {
        //                         arValues[arValues.length] = this.offers[i].TREE[index];
        //                     }

        //                     boolSearch = true;
        //                 }
        //             }
        //         }

        //         return (boolSearch ? arValues : false);
        //     },

        //     dropDownProps: function () {
        //         var target = BX.findParent(event.target, { "tag": "div", "class": "block-click" }) || event.target,
        //             skublock;

        //         skublock = BX.findNextSibling(target, { "tag": "div", "class": "internal-block" });
        //         skublock.classList.toggle('down');
        //     },

        //     quantityUp: function () {
        //         var quantity = this.obQuantity.innerHTML * 1;
        //         quantity++;
        //         this.obQuantity.innerHTML = this.quantity = quantity;
        //     },

        //     quantityDown: function () {
        //         var quantity = this.obQuantity.innerHTML * 1;

        //         if (quantity > 1) {
        //             quantity--;
        //             this.obQuantity.innerHTML = this.quantity = quantity;
        //         }
        //     },

        //     initProductData: function () {
        //         this.product.id = this.params.PRODUCT.ID;
        //         this.currentPrices = this.params.PRODUCT.ITEM_PRICES;
        //         this.currentPriceSelected = this.params.PRODUCT.ITEM_PRICE_SELECTED;
        //         this.setPrice();
        //     },

        //     initOffersData: function () {
        //         if (this.params.OFFERS && BX.type.isArray(this.params.OFFERS)) {
        //             this.offers = this.params.OFFERS;
        //             this.offerNum = 0;
        //             this.treeProps = this.params.TREE_PROPS;

        //             this.setCurrent();
        //         }
        //         else {
        //             this.errorCode = -1;
        //         }
        //     },

        //     setCurrent: function () {
        //         var i,
        //             j = 0,
        //             strName = '',
        //             arShowValues = false,
        //             arCanBuyValues = [],
        //             arFilter = {},
        //             current = this.offers[this.offerNum].TREE;

        //         for (i = 0; i < this.treeProps.length; i++) {
        //             strName = 'PROP_' + this.treeProps[i].ID;
        //             arShowValues = this.getRowValues(arFilter, strName);

        //             if (!arShowValues)
        //                 break;

        //             if (BX.util.in_array(current[strName], arShowValues)) {
        //                 arFilter[strName] = current[strName];
        //             }
        //             else {
        //                 arFilter[strName] = arShowValues[0];
        //                 this.offerNum = 0;
        //             }

        //             arCanBuyValues = arShowValues;

        //             this.updateRow(i, arFilter[strName], arShowValues, arCanBuyValues);
        //         }

        //         this.selectedValues = arFilter;
        //         this.changeInfo();
        //     },

        //     changeInfo: function () {
        //         var index = -1,
        //             j = 0,
        //             boolOneSearch = true;

        //         var i, offerGroupNode;

        //         for (i = 0; i < this.offers.length; i++) {
        //             boolOneSearch = true;

        //             for (j in this.selectedValues) {
        //                 if (this.selectedValues[j] !== this.offers[i].TREE[j]) {
        //                     boolOneSearch = false;
        //                     break;
        //                 }
        //             }

        //             if (boolOneSearch) {
        //                 index = i;
        //                 break;
        //             }
        //         }

        //         this.offerNum = index;
        //         this.currentPrices = this.offers[index].ITEM_PRICES;
        //         this.currentPriceSelected = this.offers[index].ITEM_PRICE_SELECTED;
        //         this.setPrice();
        //     },

        //     setPrice() {
        //         var price = this.currentPrices[this.currentPriceSelected];

        //         BX.adjust(this.obPrice.price, { text: price.PRINT_RATIO_PRICE });
        //         BX.adjust(this.obPrice.full, { text: price.PRINT_RATIO_BASE_PRICE });
        //     },

        allowViewedCount: function (update) {

        },

        //     initPopupWindow: function () {
        //         if (this.obPopupWin)
        //             return;

        //         this.obPopupWin = BX.PopupWindowManager.create('CatalogElementBasket_' + this.visual.ID, null, {
        //             autoHide: false,
        //             offsetLeft: 0,
        //             offsetTop: 0,
        //             overlay: true,
        //             closeByEsc: true,
        //             titleBar: true,
        //             closeIcon: true,
        //             contentColor: 'white',
        //             className: ''
        //         });
        //     },
    }
})(window);
