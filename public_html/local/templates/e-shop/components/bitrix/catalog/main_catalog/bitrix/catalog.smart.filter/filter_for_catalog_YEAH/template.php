<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
	'TEMPLATE_CLASS' => 'bx-'.$arParams['TEMPLATE_THEME']
);

if (isset($templateData['TEMPLATE_THEME']))
{
	$this->addExternalCss($templateData['TEMPLATE_THEME']);
}
?>

<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="catalog-filter-form">
	<?foreach($arResult["HIDDEN"] as $arItem):?>
	<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
	<?endforeach;?>
	<div class="catalog-filter">
		<?foreach($arResult["ITEMS"] as $key=>$arItem)//prices
		{
			if(isset($arItem["PRICE"])):
				if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
					continue;

				$step_num = 4;
				$step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / $step_num;
				$prices = array();
				if (Bitrix\Main\Loader::includeModule("currency"))
				{
					for ($i = 0; $i < $step_num; $i++)
					{
						$prices[$i] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MIN"]["VALUE"] + $step*$i, $arItem["VALUES"]["MIN"]["CURRENCY"], false);
					}
					$prices[$step_num] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MAX"]["VALUE"], $arItem["VALUES"]["MAX"]["CURRENCY"], false);
				}
				else
				{
					$precision = $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0;
					for ($i = 0; $i < $step_num; $i++)
					{
						$prices[$i] = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step*$i, $precision, ".", "");
					}
					$prices[$step_num] = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
				}
				
				if($arItem["VALUES"]["MIN"]["HTML_VALUE"]) 
					$price['min'] = $arItem["VALUES"]["MIN"]["HTML_VALUE"];
				else 
					$price['min'] = round($arItem["VALUES"]["MIN"]["VALUE"]);

				if($arItem["VALUES"]["MAX"]["HTML_VALUE"]) 
					$price['max'] = $arItem["VALUES"]["MAX"]["HTML_VALUE"];
				else 
					$price['max'] = round($arItem["VALUES"]["MAX"]["VALUE"]);	
				?>

				<div class="catalog-filter__block"> <!-- Цена -->
					<div class="catalog-filter__heading">
						<span>Цена <span>(руб.)</span></span>
					</div>
					<div class="catalog-filter__content">
						<div class="catalog-filter__price-slider" id="slider_<?=$key?>"></div>
						<input class="catalog-filter__price-val catalog-filter__price-val-min" 
								type="text"
								name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
								id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
								value="<?echo $price['min']?>"
								size="5"
								onkeyup="smartFilter.keyup(this)">
						<input class="catalog-filter__price-val catalog-filter__price-val-max" 
								type="text"
								name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
								id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
								value="<?echo $price['max']?>"
								size="5"
								onkeyup="smartFilter.keyup(this)">
					</div>
					<script>
						$(document).ready(function() {
							var price_min_<?=$key?> = <?echo $arItem["VALUES"]["MIN"]["VALUE"]?>,
								price_max_<?=$key?> = <?echo $arItem["VALUES"]["MAX"]["VALUE"]?>,
								html_price_min_<?=$key?> = <?echo $price['min']?>,
								html_price_max_<?=$key?> = <?echo $price['max']?>;

							var catalogFilterPriceSlider = $('#slider_<?=$key?>').slider({
								range: true,
								min: price_min_<?=$key?>,
								max: price_max_<?=$key?>,
								values: [ html_price_min_<?=$key?>, html_price_max_<?=$key?> ],
								slide: function( event, ui ) {
									$('#slider_<?=$key?>').parent('.catalog-filter__content').find('.catalog-filter__price-val-min').val(ui.values[0]);
									$('#slider_<?=$key?>').parent('.catalog-filter__content').find('.catalog-filter__price-val-max').val(ui.values[1]);
								}
							});

							$("#slider_<?=$key?>").slider({
								change: function(event, ui) {
									$('#slider_<?=$key?>').parent('.catalog-filter__content').find('.catalog-filter__price-val').each(function(){
										smartFilter.keyup(this);
									});
								}
							});

							$('#slider_<?=$key?> .catalog-filter__price-val').on('change', function() {
								var catalogFilterPriceValues = Array();
								$('#slider_<?=$key?>').parent('.catalog-filter__content').find('.catalog-filter__price-val').each(function(){
									catalogFilterPriceValues.push($(this).val());
								})
								catalogFilterPriceSlider.slider('option', 'values', catalogFilterPriceValues);
								delete catalogFilterPriceValues;
							});
						});
					</script>
				</div>
				<?echo "<pre>"; print_r($arItem); echo "</pre>";?>
			<?endif;
		}

		//not prices
		foreach($arResult["ITEMS"] as $key=>$arItem)
		{
			if(
				empty($arItem["VALUES"])
				|| isset($arItem["PRICE"])
			)
				continue;

			if (
				$arItem["DISPLAY_TYPE"] == "A"
				&& (
					$arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
				)
			)
				continue;
			?>
			<div class="catalog-filter__block">
				<div class="catalog-filter__heading">
					<span><?=$arItem['NAME']?></span>
				</div>
				<div class="catalog-filter__content">
					<?
					$arCur = current($arItem["VALUES"]);
					switch ($arItem["DISPLAY_TYPE"])
					{
						case "A"://NUMBERS_WITH_SLIDER
							
							if($arItem["VALUES"]["MIN"]["HTML_VALUE"]) 
								$price['min'] = $arItem["VALUES"]["MIN"]["HTML_VALUE"];
							else 
								$price['min'] = round($arItem["VALUES"]["MIN"]["VALUE"]);

							if($arItem["VALUES"]["MAX"]["HTML_VALUE"]) 
								$price['max'] = $arItem["VALUES"]["MAX"]["HTML_VALUE"];
							else 
								$price['max'] = round($arItem["VALUES"]["MAX"]["VALUE"]);
							
							?>
							<div class="catalog-filter__price-slider" id="slider_<?=$key?>"></div>
							<input class="catalog-filter__price-val catalog-filter__price-val-min" 
									type="text"
									name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
									id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
									value="<?echo $price['min']?>"
									size="5"
									onkeyup="smartFilter.keyup(this)">
							<input class="catalog-filter__price-val catalog-filter__price-val-max" 
									type="text"
									name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
									id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
									value="<?echo $price['max']?>"
									size="5"
									onkeyup="smartFilter.keyup(this)">
							<script>
								$(document).ready(function() {
									var price_min_<?=$key?> = <?echo $arItem["VALUES"]["MIN"]["VALUE"]?>,
										price_max_<?=$key?> = <?echo $arItem["VALUES"]["MAX"]["VALUE"]?>,
										html_price_min_<?=$key?> = <?echo $price['min']?>,
										html_price_max_<?=$key?> = <?echo $price['max']?>;

									var catalogFilterPriceSlider = $('#slider_<?=$key?>').slider({
										range: true,
										min: price_min_<?=$key?>,
										max: price_max_<?=$key?>,
										values: [ html_price_min_<?=$key?>, html_price_max_<?=$key?> ],
										slide: function( event, ui ) {
											$('#slider_<?=$key?>').parent('.catalog-filter__content').find('.catalog-filter__price-val-min').val(ui.values[0]);
											$('#slider_<?=$key?>').parent('.catalog-filter__content').find('.catalog-filter__price-val-max').val(ui.values[1]);
										}
									});

									$("#slider_<?=$key?>").slider({
										change: function(event, ui) {
											$('#slider_<?=$key?>').parent('.catalog-filter__content').find('.catalog-filter__price-val').each(function(){
												smartFilter.keyup(this);
											});
										}
									});

									$('#slider_<?=$key?> .catalog-filter__price-val').on('change', function() {
										var catalogFilterPriceValues = Array();
										$('#slider_<?=$key?>').parent('.catalog-filter__content').find('.catalog-filter__price-val').each(function(){
											catalogFilterPriceValues.push($(this).val());
										})
										catalogFilterPriceSlider.slider('option', 'values', catalogFilterPriceValues);
										delete catalogFilterPriceValues;
									});
								});
							</script>
							<?
							
							break;
						case "B"://NUMBERS
							?>
							<div class="col-xs-6 bx-filter-parameters-box-container-block bx-left">
								<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_FROM")?></i>
								<div class="bx-filter-input-container">
									<input
										class="min-price"
										type="text"
										name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
										id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
										value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
										size="5"
										onkeyup="smartFilter.keyup(this)"
										/>
								</div>
							</div>
							<div class="col-xs-6 bx-filter-parameters-box-container-block bx-right">
								<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_TO")?></i>
								<div class="bx-filter-input-container">
									<input
										class="max-price"
										type="text"
										name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
										id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
										value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
										size="5"
										onkeyup="smartFilter.keyup(this)"
										/>
								</div>
							</div>
							<?
							break;
						case "G"://CHECKBOXES_WITH_PICTURES
							?>
							<div class="col-xs-12">
								<div class="bx-filter-param-btn-inline">
								<?foreach ($arItem["VALUES"] as $val => $ar):?>
									<input
										style="display: none"
										type="checkbox"
										name="<?=$ar["CONTROL_NAME"]?>"
										id="<?=$ar["CONTROL_ID"]?>"
										value="<?=$ar["HTML_VALUE"]?>"
										<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
									/>
									<?
									$class = "";
									if ($ar["CHECKED"])
										$class.= " bx-active";
									if ($ar["DISABLED"])
										$class.= " disabled";
									?>
									<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'bx-active');">
										<span class="bx-filter-param-btn bx-color-sl">
											<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
											<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
											<?endif?>
										</span>
									</label>
								<?endforeach?>
								</div>
							</div>
							<?
							break;
						case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
							?>
							<div class="col-xs-12">
								<div class="bx-filter-param-btn-block">
								<?foreach ($arItem["VALUES"] as $val => $ar):?>
									<input
										style="display: none"
										type="checkbox"
										name="<?=$ar["CONTROL_NAME"]?>"
										id="<?=$ar["CONTROL_ID"]?>"
										value="<?=$ar["HTML_VALUE"]?>"
										<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
									/>
									<?
									$class = "";
									if ($ar["CHECKED"])
										$class.= " bx-active";
									if ($ar["DISABLED"])
										$class.= " disabled";
									?>
									<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'bx-active');">
										<span class="bx-filter-param-btn bx-color-sl">
											<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
												<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
											<?endif?>
										</span>
										<span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
										if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
											?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
										endif;?></span>
									</label>
								<?endforeach?>
								</div>
							</div>
							<?
							break;
						case "P"://DROPDOWN
							$checkedItemExist = false;
							?>
							<div class="col-xs-12">
								<div class="bx-filter-select-container">
									<div class="bx-filter-select-block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
										<div class="bx-filter-select-text" data-role="currentOption">
											<?
											foreach ($arItem["VALUES"] as $val => $ar)
											{
												if ($ar["CHECKED"])
												{
													echo $ar["VALUE"];
													$checkedItemExist = true;
												}
											}
											if (!$checkedItemExist)
											{
												echo GetMessage("CT_BCSF_FILTER_ALL");
											}
											?>
										</div>
										<div class="bx-filter-select-arrow"></div>
										<input
											style="display: none"
											type="radio"
											name="<?=$arCur["CONTROL_NAME_ALT"]?>"
											id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
											value=""
										/>
										<?foreach ($arItem["VALUES"] as $val => $ar):?>
											<input
												style="display: none"
												type="radio"
												name="<?=$ar["CONTROL_NAME_ALT"]?>"
												id="<?=$ar["CONTROL_ID"]?>"
												value="<? echo $ar["HTML_VALUE_ALT"] ?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
											/>
										<?endforeach?>
										<div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none;">
											<ul>
												<li>
													<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx-filter-param-label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
														<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
													</label>
												</li>
											<?
											foreach ($arItem["VALUES"] as $val => $ar):
												$class = "";
												if ($ar["CHECKED"])
													$class.= " selected";
												if ($ar["DISABLED"])
													$class.= " disabled";
											?>
												<li>
													<label for="<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" data-role="label_<?=$ar["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')"><?=$ar["VALUE"]?></label>
												</li>
											<?endforeach?>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<?
							break;
						case "R"://DROPDOWN_WITH_PICTURES_AND_LABELS
							?>
							<div class="col-xs-12">
								<div class="bx-filter-select-container">
									<div class="bx-filter-select-block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
										<div class="bx-filter-select-text fix" data-role="currentOption">
											<?
											$checkedItemExist = false;
											foreach ($arItem["VALUES"] as $val => $ar):
												if ($ar["CHECKED"])
												{
												?>
													<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
														<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
													<?endif?>
													<span class="bx-filter-param-text">
														<?=$ar["VALUE"]?>
													</span>
												<?
													$checkedItemExist = true;
												}
											endforeach;
											if (!$checkedItemExist)
											{
												?><span class="bx-filter-btn-color-icon all"></span> <?
												echo GetMessage("CT_BCSF_FILTER_ALL");
											}
											?>
										</div>
										<div class="bx-filter-select-arrow"></div>
										<input
											style="display: none"
											type="radio"
											name="<?=$arCur["CONTROL_NAME_ALT"]?>"
											id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
											value=""
										/>
										<?foreach ($arItem["VALUES"] as $val => $ar):?>
											<input
												style="display: none"
												type="radio"
												name="<?=$ar["CONTROL_NAME_ALT"]?>"
												id="<?=$ar["CONTROL_ID"]?>"
												value="<?=$ar["HTML_VALUE_ALT"]?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
											/>
										<?endforeach?>
										<div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none">
											<ul>
												<li style="border-bottom: 1px solid #e5e5e5;padding-bottom: 5px;margin-bottom: 5px;">
													<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx-filter-param-label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
														<span class="bx-filter-btn-color-icon all"></span>
														<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
													</label>
												</li>
											<?
											foreach ($arItem["VALUES"] as $val => $ar):
												$class = "";
												if ($ar["CHECKED"])
													$class.= " selected";
												if ($ar["DISABLED"])
													$class.= " disabled";
											?>
												<li>
													<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')">
														<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
															<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
														<?endif?>
														<span class="bx-filter-param-text">
															<?=$ar["VALUE"]?>
														</span>
													</label>
												</li>
											<?endforeach?>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<?
							break;
						case "K"://RADIO_BUTTONS
							?>
							<div class="col-xs-12">
								<div class="radio">
									<label class="bx-filter-param-label" for="<? echo "all_".$arCur["CONTROL_ID"] ?>">
										<span class="bx-filter-input-checkbox">
											<input
												type="radio"
												value=""
												name="<? echo $arCur["CONTROL_NAME_ALT"] ?>"
												id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
												onclick="smartFilter.click(this)"
											/>
											<span class="bx-filter-param-text"><? echo GetMessage("CT_BCSF_FILTER_ALL"); ?></span>
										</span>
									</label>
								</div>
								<?foreach($arItem["VALUES"] as $val => $ar):?>
									<div class="radio">
										<label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
											<span class="bx-filter-input-checkbox <? echo $ar["DISABLED"] ? 'disabled': '' ?>">
												<input
													type="radio"
													value="<? echo $ar["HTML_VALUE_ALT"] ?>"
													name="<? echo $ar["CONTROL_NAME_ALT"] ?>"
													id="<? echo $ar["CONTROL_ID"] ?>"
													<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
													onclick="smartFilter.click(this)"
												/>
												<span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
												if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
													?>&nbsp;(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
												endif;?></span>
											</span>
										</label>
									</div>
								<?endforeach;?>
							</div>
							<?
							break;
						case "U"://CALENDAR
							?>
							<div class="col-xs-12">
								<div class="bx-filter-parameters-box-container-block"><div class="bx-filter-input-container bx-filter-calendar-container">
									<?$APPLICATION->IncludeComponent(
										'bitrix:main.calendar',
										'',
										array(
											'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
											'SHOW_INPUT' => 'Y',
											'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MIN"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
											'INPUT_NAME' => $arItem["VALUES"]["MIN"]["CONTROL_NAME"],
											'INPUT_VALUE' => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
											'SHOW_TIME' => 'N',
											'HIDE_TIMEBAR' => 'Y',
										),
										null,
										array('HIDE_ICONS' => 'Y')
									);?>
								</div></div>
								<div class="bx-filter-parameters-box-container-block"><div class="bx-filter-input-container bx-filter-calendar-container">
									<?$APPLICATION->IncludeComponent(
										'bitrix:main.calendar',
										'',
										array(
											'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
											'SHOW_INPUT' => 'Y',
											'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MAX"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
											'INPUT_NAME' => $arItem["VALUES"]["MAX"]["CONTROL_NAME"],
											'INPUT_VALUE' => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
											'SHOW_TIME' => 'N',
											'HIDE_TIMEBAR' => 'Y',
										),
										null,
										array('HIDE_ICONS' => 'Y')
									);?>
								</div></div>
							</div>
							<?
							break;
						default://CHECKBOXES
							switch ($arItem['CODE']) {
								case 'size':
									foreach($arItem["VALUES"] as $val => $ar):?>
										<label data-role="label_<?=$ar["CONTROL_ID"]?>" class="catalog-filter__size-checkbox" for="<? echo $ar["CONTROL_ID"] ?>">
										<input
												type="checkbox"
												value="<? echo $ar["HTML_VALUE"] ?>"
												name="<? echo $ar["CONTROL_NAME"] ?>"
												id="<? echo $ar["CONTROL_ID"] ?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												onclick="smartFilter.click(this)"
											/>
											<span title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?></span>
										</label>
									<?endforeach;
									break;
								case 'color':
									foreach($arItem["VALUES"] as $val => $ar):?>
										<label class="catalog-filter__color-checkbox" for="<? echo $ar["CONTROL_ID"] ?>">
		<input type="text">
											<input
												type="checkbox"
												value="<? echo $ar["HTML_VALUE"] ?>"
												name="<? echo $ar["CONTROL_NAME"] ?>"
												id="<? echo $ar["CONTROL_ID"] ?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												onclick="smartFilter.click(this)"
											/>
											<i class="icon"></i>
											<img src="<?=$ar['FILE']['SRC']?>" alt="<?=$ar["VALUE"];?>"/>
											<div class="popup"><?=$ar["VALUE"];?></div>
										</label>
									<?endforeach;
									break;
								default:
									foreach($arItem["VALUES"] as $val => $ar):?>
										<?//echo "<pre>"; print_r($ar); echo "</pre>";?>
										<label class="catalog-filter-checkbox" for="<? echo $ar["CONTROL_ID"] ?>">
											<input
												type="checkbox"
												value="<? echo $ar["HTML_VALUE"] ?>"
												name="<? echo $ar["CONTROL_NAME"] ?>"
												id="<? echo $ar["CONTROL_ID"] ?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												onclick="smartFilter.click(this)"
											/>

											<span title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?></span>
										</label>
									<?endforeach;
									break;
							}
							break;
					}
					?>
				</div>
			</div>
		<?
		}
		?>
	</div><!--//row-->

	<input
		type="hidden"
		id="set_filter"
		name="set_filter"
		value="<?=GetMessage("CT_BCSF_SET_FILTER")?>"
	/>
	

	<a id="button-filter" class="catalog-filter-form__submit" href="<?echo $arResult["FILTER_URL"]?>" target="">Показать <span id="num_filter"><?=$GLOBALS['PRODUCTS_COUNT']?></span> товаров</a>
	<?if (siteType == "mobile"):?>
		<input class="catalog-filter-form__reset" type="reset" onclick="smartFilter.click(this);" value="ОЧИСТИТЬ ФИЛЬТР">
	<?endif;?>
</form>

<script type="text/javascript">
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>, "<?=siteType?>");
</script>