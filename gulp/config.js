const root = {
    src: 'public_html/local/templates/main',
    dist: 'public_html/local/templates/main'
}

module.exports = {
    assets: {
        styles: {
            src: [
                root.src + '/src/sass/**/*.scss'
            ],
            entry: root.src + '/src/sass/**/*.scss',
            output: root.dist + '/styles',
            options: {
            }
        }
    }
}