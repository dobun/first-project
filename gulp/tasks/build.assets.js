var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    notify = require('gulp-notify');

var config = require('../config');

gulp.task('build:assets:styles', function(){
    return gulp.src(config.assets.styles.entry)
    .pipe(plumber({
        errorHandler: notify.onError(err => ({
            title: 'Assets : Styles',
            message: err.message
        }))
    }))
    .pipe(sass(config.assets.styles.options))
    .pipe(gulp.dest(config.assets.styles.output))
});