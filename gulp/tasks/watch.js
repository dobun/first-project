var gulp = require('gulp');
var config = require('../config.js');

gulp.task('watch', gulp.series(
    'build',
    gulp.parallel(
        function () {
            gulp.watch(config.assets.styles.src, gulp.parallel('build:assets:styles'));
        }
    )
));